import telebot
import yfinance as yf
from flask import Flask , request
import os

Toekn = "5361317962:AAEZ4e8AHE0lBqKjlvJ6jjRdKiegWWlVLR0"

bot = telebot.TeleBot(token=Toekn)
server = Flask(__name__)


@bot.message_handler(commands=['hello'])
def hello(message):
    bot.send_message(message.chat.id,"Hey Arshiya How are you doing today?")

"""Show stock market"""
@bot.message_handler(commands=['price'])
def show_stock(message):
    response =""
    stocks= ['gme','amc','nok']
    stock_data = []
    for stock in stocks:
        data = yf.download(tickers=stock,period='2d',interval='1d')
        data = data.reset_index()
        response += f"----{stock}----\n"
        stock_data.append([stock])
        columns = ['stock']
        for index,row in data.iterrows():
            stock_position = len(stock_data) - 1
            price = round(row['Close'],2)
            format_date = row['Date'].strftime('%m/%d')
            response += f"{format_date}: {price}\n"
            stock_data[stock_position].append(price)
            columns.append(format_date)
        print()

    response = f"{columns[0] : <10}{columns[1] : ^10}{columns[2] : >10}\n"
    for row in stock_data:
        response += f"{row[0] : <10}{row[1] : ^10}{row[2] : >10}\n"
    response += "\nstock data by Arshiya"
    print(response)
    bot.send_message(message.chat.id,response)


@server.route('/' + bot, methods = ['Post'])
def get_message():
    bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])

@server.route("/")
def webhook():
    bot.remove_webhook()
    bot.set_webhook(url="https://enigmatic-sea-89140.herokuapp.com/" + Toekn)
    return "!",200

if __name__ == "__main__":
    server.run(host="0.0.0.0",port=int(os.environ.get('PORT',5000)))
